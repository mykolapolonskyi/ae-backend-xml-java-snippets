# AgileEngine backend-XML java snippets

It is built on top of [Jsoup](https://jsoup.org/).

You can use Jsoup for your solution or apply any other convenient library. 


TLDR On results:

```
~/p/s/ae-backend-xml-java-snippets    java -jar build/libs/ae-backend-xml-java-snippets-0.0.1.jar samples/startbootstrap-freelancer-gh-pages-cut.html                                                                                332ms  Sun Oct 20 18:09:41 2019
[ERROR] 2019-10-20 18:17:05,314 c.a.Main - For file samples/startbootstrap-freelancer-gh-pages-cut.htmlgot an error: Can't find any similar element
```

```
 ~/p/s/ae-backend-xml-java-snippets    java -jar build/libs/ae-backend-xml-java-snippets-0.0.1.jar src/test/resources/pages/*                                                                                                         334ms  Sun Oct 20 18:09:37 2019

[INFO] 2019-10-20 18:09:41,124 c.a.Main - /Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-0-origin.html: #root/html/body/div/div/div/div/div/div/a
[INFO] 2019-10-20 18:09:41,125 c.a.Main - /Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-1-evil-gemini.html: #root/html/body/div/div/div/div/div/div/a
[INFO] 2019-10-20 18:09:41,125 c.a.Main - /Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-2-container-and-clone.html: #root/html/body/div/div/div/div/div/div/div/a
[INFO] 2019-10-20 18:09:41,125 c.a.Main - /Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-3-the-escape.html: #root/html/body/div/div/div/div/div/div/a
[INFO] 2019-10-20 18:09:41,125 c.a.Main - /Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-4-the-mash.html: #root/html/body/div/div/div/div/div/div/a
/Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-0-origin.html: #root/html/body/div/div/div/div/div/div/a
/Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-1-evil-gemini.html: #root/html/body/div/div/div/div/div/div/a
/Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-2-container-and-clone.html: #root/html/body/div/div/div/div/div/div/div/a
/Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-3-the-escape.html: #root/html/body/div/div/div/div/div/div/a
/Users/mykolapolonskyi/projects/samples/ae-backend-xml-java-snippets/src/test/resources/pages/sample-4-the-mash.html: #root/html/body/div/div/div/div/div/div/a

```