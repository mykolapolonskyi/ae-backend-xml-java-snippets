buildscript {
    val kotlinVersion = "1.3.50"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    }
}

group = "com.agileengine"
version = "0.0.1"

plugins {
    val kotlinVersion = "1.3.50"
    java
    kotlin("jvm") version kotlinVersion
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jsoup:jsoup:1.11.2")
    implementation("org.slf4j:slf4j-log4j12:1.7.25")
    testImplementation("org.junit.jupiter:junit-jupiter:5.5.2")
    testImplementation(kotlin("stdlib-jdk8"))
    testImplementation(kotlin("test"))
}

val jar by tasks.getting(Jar::class) {
    manifest {
        attributes["Main-Class"] = "com.agileengine.Main"
    }
    configurations {
        compileClasspath {
            files.forEach {
                from(zipTree(it.absoluteFile))
            }
        }
    }
}


tasks {
    val copyManifest = register<Copy>("copy-jar") {
        from("build/libs/ae-backend-xml-java-snippets-0.0.1.jar")
        into("./")
    }
    val build by getting {
        dependsOn(copyManifest)
    }
    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }
}

