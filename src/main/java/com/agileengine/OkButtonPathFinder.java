package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class OkButtonPathFinder {
    private static Logger LOGGER = LoggerFactory.getLogger(OkButtonPathFinder.class);

    private static String CHARSET_NAME = "utf8";

    private String id = "make-everything-ok-button";
    private String clazz = "btn-success";
    private String title = "Make-Button";
    private String hrefPattern = "ok";


    public String calculatePath(File file) throws IOException {
        String absoluteFilePath = file.getAbsolutePath();
        LOGGER.debug("file {} was loaded", absoluteFilePath);
        Document doc = Jsoup.parse(file, CHARSET_NAME, absoluteFilePath);

        Optional<Element> optionalElement = tryToGetOkButton(doc);

        LinkedList<Tag> path = new LinkedList<>();

        if (!optionalElement.isPresent()) {
            throw new IllegalArgumentException("Can't find any similar element");
        }

        Element element = optionalElement.get();
        path.add(0, element.tag());

        Element parent = element.parent();
        while (parent != null) {
            path.add(0, parent.tag());
            parent = parent.parent();
        }

        String result = path.stream().map(Tag::toString).collect(Collectors.joining("/"));
        LOGGER.debug("button pass: {}", result);
        return result;
    }

    private Optional<Element> tryToGetOkButton(Document doc) {
        Element el = doc.getElementById(id);
        List<Element> elements;
        if (el == null) { // class search
            el = tryToGetOne(doc.getElementsByClass(clazz));
        }
        if (el == null) {//title
            el = tryToGetOne(doc.getElementsByAttributeValue("title", title));
        }
        if (el == null) {//href
            el = tryToGetOne(doc.getElementsByAttributeValueContaining("href", hrefPattern));
        }
        return Optional.ofNullable(el);
    }

    private Element tryToGetOne(List<Element> elements) {
        if (elements.size() == 1) {
            return elements.get(0);
        } else {
            List<Element> filtred = elements.stream()
                    .filter(it -> it.attr("class").contains(clazz)
                            && it.attr("href").contains(hrefPattern)
                            && it.attr("title").contains(title)
                    ).collect(Collectors.toList());
            if (filtred.size() == 1)
                return filtred.get(0);
            else
                return null;
        }
    }
}
