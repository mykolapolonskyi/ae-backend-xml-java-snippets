package com.agileengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    private static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        ArrayList<String> results = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            try {
                File file = new File(args[i]);
                String result = new OkButtonPathFinder().calculatePath(file);
                results.add(file.getAbsolutePath() + ": " + result);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            } catch (IllegalArgumentException e) {
                LOGGER.error("For file " + args[0] + "got an error: " + e.getMessage());
            }
        }
        results.forEach(s -> LOGGER.info("{}", s));

        results.forEach(System.out::println);
    }
}