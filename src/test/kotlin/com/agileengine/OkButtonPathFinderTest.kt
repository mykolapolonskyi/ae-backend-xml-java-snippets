package com.agileengine

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertTrue

//Test case
data class Case(val filePath: String, val expectedPath: String)

class OkButtonPathFinderTest {
    private val pathFinder = OkButtonPathFinder()

    fun cases(): List<Case> = listOf(
        Case(expectedPath = "#root/html/body/div/div/div/div/div/div/a", filePath = "pages/sample-0-origin.html"),
        Case(expectedPath = "#root/html/body/div/div/div/div/div/div/a", filePath = "pages/sample-1-evil-gemini.html"),
        Case(expectedPath = "#root/html/body/div/div/div/div/div/div/div/a", filePath = "pages/sample-2-container-and-clone.html"),
        Case(expectedPath = "#root/html/body/div/div/div/div/div/div/a", filePath = "pages/sample-3-the-escape.html"),
        Case(expectedPath = "#root/html/body/div/div/div/div/div/div/a", filePath = "pages/sample-4-the-mash.html")
    )

    @ParameterizedTest
    @MethodSource("cases")
    fun `calculate path`(case: Case) {
        val result = pathFinder.calculatePath(File(javaClass.classLoader.getResource(case.filePath).file))
        assertEquals(case.expectedPath, result)
    }
}